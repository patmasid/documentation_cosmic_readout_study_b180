### How to generate a partition

```
cd /afs/cern.ch/user/n/nswdaq/workspace/public/stgc_b180/nswpartitionmaker/partitionConfigs/
source /afs/cern.ch/user/n/nswdaq/workspace/public/nswdaq/current/nswdaq/dev-installed/setup_pm.sh
makeNSWPartition.py 180-sTGC_Pulser_ExtL1_swrod.yml (or any yml file you want)
```


### How to start IGUI

```
ssh -Y nswdaq@um-felix3-swrod
cd /afs/cern.ch/user/n/nswdaq/workspace/public/stgc_b180/nswpartitionmaker/partitionConfigs/180-sTGC_Pulser_ExtL1_swrod
source setup_part.sh
run
```

Closing the IGUI and shutting down the partition:
    Go to the ```File``` menu in the ```IGUI``` and then select ```Close IGUI & exit partition```. 
    Never select the ```x``` in the top right corner. However if you do that, then run ```run``` again and gain control of the system by going to ```Access Control --> Control (instead of Display)``` 
* Killing the partition (abnormal ways to shut down the partition. Try not to do it.) \
    ```pmg_kill_partition -p part-180-sTGC_Pulser; pkill -ec -f part-180-sTGC_Pulser; rmgr_free_partition_resources -p part-180-sTGC_Pulser```

* Instructions for changing some parameters by not closing the IGUI or the partition:
    * https://stgc-comm-docs.web.cern.ch/elx/noise_test/
    
    
### How to analyze the cosmic run:

```
ssh -Y (swrod machine you took the data) stgcic@um-felix3-swrod
cd /data/swrod_data/cosmic_runs/high_voltage_Q2-2800V/runX_<runname>/
source /afs/cern.ch/user/n/nswdaq/workspace/public/nswdaq/current/nswdaq/<the appropriate release e.g. unstable-installed or dev-installed>/setup_nswdaq.sh
simple data*.RAW.*.data

/afs/cern.ch/work/p/patmasid/public/swROD/swROD_analysis/x86_64-centos7-gcc8-opt/felixdata_minitreeanalysis/analyzeHit data_test.*._.daq.RAW.*.simple.root /afs/cern.ch/work/p/patmasid/public/swROD/swROD_analysis/felixdata_minitreeanalysis/data/ElinkFebFiberMapping/2Elinks_EachFEB/ElinkFebFiberMapping_wedgeTest_320Link.txt ./data_test.*._.daq.RAW.*.decoded.root L P 20MNIWLCP00006 ./ TP ELinkID
```
    
### How to take noise scan:



### How to generate the hit-map of the wedge using Man's script:

### How to decode binary data from swrod:



### How to decode data from netio:
```/eos/atlas/atlascerngroupdisk/det-nsw-stgc/swrod_b180/20MNIWLCP00006/cosmics/netio/for_setting_flx_bcid_offset/netio_output_decoding.txt```


### How to take pdo-tdo calibration:

