* Creating a sudo user:
    * ```sudo usermod -aG wheel username```
* Creating a user-group:
    * ```sudo groupadd nswdet```
    * ```sudo usermod -a -G nswdet nswdaq```
    * ```sudo usermod -a -G nswdet stgcic```
* Giving ownership to a group:
    * ```chmod g+rx /data/```