# Problems with the test-pulse data taken with swrod

* Date: 10-06-2021
    1. atlas-nsw-stgc-daq1 is very slow
        * Trying to work with um-felix3-swrod. root password: umfelex3
            * Could not be fixed. 
            * Also tried with um-felix2-swrod. Can generate partition and run it on this machine, but it is as slow as atlas-nsw-stgc-daq1.
            * Finally, decided to use atlas-nsw-stgc-daq1 only.
            * (29-07-2021) Created a ticket with IT department. The problem is fixed and swrod machine can be used now. 
    2. We are losing random number of packets when fixed number of test-pulses sent 
        * Try with 20 sec delay.
            * Does not work everytime. Works only few times. Not well understood.
        * Find the way to work with ```menuAltiModule```.
            * In SBC Machine after rootController is in ```RUNNING``` state.
              ```
              source /ATLAS/current/setup_alti.sh
              menuAltiModule
              7
              4
              0
              3
              ```
              The above mentioned problem not solved by this.
              
    3. Also missing packets in continuous test-pulse readout
        * Try 20 sec delay.
    4. One wedge (LCC6) PFEB L4Q2 has low efficiency VMM 0 but netio output seems to be okay.
    5. Have to do noise scan.
    
    6.  (26-07-2021) Cannot see muon pulse from the wedge along with the trigger on scope:
        1. Try changing the width of the coincidence pulse
        2. Try different channels (mostly central region of Q2) - This worked!!!
        3. Maybe something to do with the threshold value set in the discriminator? Play with it if necessa
    7. (27-07-2021) Tuning the offsets (SFEB VMM5 L0 offset has to be one less than that for other VMMs on SFEB)
        1. Works for SFEB: ch tagging offset: 40, L0 offset: 13
        2. Works for PFEB: ch tagging offset: 40, L0 offset: 14  
    8. (27-07-2021) Some 0 pdo many many hits on SFEB Q2 Layer 1 and 2 VMM 0.
        * We do not see this anymore.
    9. Elink has wrong checksum error while taking cosmic data (the reason is not yet fully understood)
        * (30-07-2021) This error disappeared when ch tagging offset was changed from 40 to 940 and l0 offset changed from 13 to 913. 
    10. (30-07-2021) 16 instead of 48 packets received error appears when L0 offset is set differently for SFEB and PFEB. 
        * This is solved by changing ch tagging offset and not L0 offset.
        
    11. (30-07-2021) Current offsets:
        1. Works for SFEB: ch tagging offset: 940, L0 offset: 913, ROC bc offset: 913 (only SFEBs VMM5 l0 offset is 912 - one bc less than others' l0 offset. everything else stays the same)
        2. Works for PFEB: ch tagging offset: 939, L0 offset: 913
        
    12. (30-07-2021) Things have to be done before the final configuration:
        1. Threshold has to be set (for both discriminator and the VMMs) after noise and hit rate scans
        2. Widths of the discriminator and the coincidence have to be changed to the maximum possible number.
        3. Using swrod with useTTC mode and FELIX bcid offset has to be set. Make sure you use the correct ttcelink. Please refer to the section "Setting offsets for L0 and L1 matching" in Meetings.md.
        4. Make sure that all trimmed thresholds are correct. (connected PFEB channels do not have the correct offset due to lower baseline value from SCA-ADC due to the higher noise. See: https://docs.google.com/spreadsheets/d/1GT4h7_1kpLd4RXntkUGWKJLVeFm_bHNhwZ9KUXQlNe4/edit#gid=0).
        
    13. (30-07-2021) For the things that have to be done mentioned in the bullet point, some pre-studies are needed
        1. Noise rate scan
        2. Hit rate scan
        
    14. More general studies that can be performed:
        1. Hit map and correlation with different layers
        2. Tdo Pdo Calibration
    
