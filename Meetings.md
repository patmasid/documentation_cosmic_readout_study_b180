# NSW Electronics Integration and Commissioning - Testing the readout at B180
# Cosmic Ray Readout Tests - Minutes of the meetings
# //-----------------------------------------------------------------------//
#   Discussion Minutes:

* [How to access the notebook](#how-to-access-the-notebook)
* [Rongkun - swROD](#rongkun-swROD)
* [Xiong - First Results](#xiong-first-results)
* [Xiong - miniDAQ pad trigger](#xiong-minidaq-pad-trigger)
* [Liang - Tasks](#liang-tasks)
* [Rongkun - b191 Setup](#rongkun-b191-setup)
* [Installation of LINUX CERN CENTOS7](#installation-of-linux-cern-centos7)
* [Requesting a fixed IP address](requesting-a-fixed-ip-address)
* [Access using Kerberos and ssh](access-using-kerberos-and-ssh)
* [Jared Sturdy - About TDAQ/NSWDAQ releases](#jared_sturdy-about-tdaq/nswdaq-releases)
* [About ALTI](#about-alti)
* [About Information Service](#about-information-service)
* [Starting and stopping a partition](#starting-and-stopping-a-partition)
* [Debugging](#debugging)
* [Enrico - Debugging the VME crate and ALTI setup on SBC machine](#enrico-debugging-the-vme-crate-and-alti-setup-on-sbc-machine)
* [Problem in configuration of the boards through IGUI](#problem-in-configuration-of-the-boards-through-IGUI)
* [Analysing the data from the swrod run](#analysing-the-data-from-the-swrod-run)
* [Setting up CAEN Power supply modules](#setting-up-caen-power-supply-modules)
* [Setting offsets for L0 and L1 matching](#setting-offsets-for-l0-and-l1-matching)

## How to access the notebook

* Setting up the station:
  ```ssh -v -N -L 23332:localhost:8888 nsw@atlas-nsw-stgc-daq1``` (any number in place of 23332)
* Type the following in your browser: 
  ```localhost:23332```

## Rongkun - swROD

<img src='./images/swROD/1618411342952.JPEG' width="400" height="250">

For sending specific number of test-pulses, in the file /afs/cern.ch/user/x/xuwa/public/cont_altiPat_stgc.dat : 

```
#-----------------------
#                      M
#                      u
#                      l
#                      t
#                      i
#                      p
#                      l
#   CCC                i
#  BRRR    T BBBB TTT  c
#O UEEE    T GGGG TTTL i
#R SQQQ    Y OOOO RRR1 t
#B Y210    P 3210 321A y
#-----------------------
# comment to disable OCR
 1 0000 0x00 1000 0000 1
# 0 0000 0x00 0000 0000 50
# 1 0000 0x00 0000 0000 1
# 0 0000 0x00 0000 0000 100
# 0 0000 0x00 0010 0000 1
 0 0000 0x00 0000 0000 200
# comment to disable BCR
 0 0000 0x00 0001 0000 1 # was bgo0
 0 0000 0x00 0000 0000 100
# comment the next line to disable TP test pulse
 0 0000 0x00 0100 0000 1
 0 0000 0x00 0000 0000 24
 0 0000 0x00 0000 0000 66
 0 0000 0x00 0000 0001 1
 # 0 0000 0x00 0000 0000 34720000
# 0 0000 0x00 0000 0000 34720000 # 1.1 Hz
# 0 0000 0x00 0000 0000 3472000  # 11 Hz
 0 0000 0x00 0000 0000 347200   # 110 Hz
# 0 0000 0x00 0000 0000 34720    # 1.1 kHz
# 0 0000 0x00 0000 0000 3472     # 11 kHz
# 0 0000 0x00 0000 0000 347      # 110 kHz
```

Instead of sending test-pulses together, copy the lines 100 times to get 100 test-pulses. Each segment of the file will send only one test-pulse.

## Xiong - First Results

Results with only CO2:

https://docs.google.com/presentation/d/1m1k4qaqrNXeTFQfwnF37IsDwyGH9B47CIrqvjFwHb_Y/edit#slide=id.p1

https://docs.google.com/presentation/d/1gWs1ffUi4II0YiON9osKqdzwwQaso5crh9rR-GNbX3k/edit#slide=id.g9e12191d70_2_80

Xiong's presentation in NSW General Meeting:
https://indico.cern.ch/event/1021352/contributions/4286863/attachments/2216517/3752536/20210326_NSW_stgc_cosmic.pdf

Code used by Xiong and Man:

1. ```/afs/cern.ch/work/s/stgcic/public/miscelleneous/stgc_cosmic_mc/```
2. https://gitlab.cern.ch/mayuan/stgc_cosmic_ray_test/-/tree/master/cosmic_ana/detector_mapping

## Xiong - miniDAQ pad trigger

Xiong is developing pad trigger logic using miniDAQ. He is creating a mapping between 128 channels of pad TDS with the most efficient logic pads.

## Liang - Tasks
* **Main goal:**
    * Test the wedge trigger + readout together
    * Do timing studies with the readout chain
* **Understanding**
    * Data flow
    * swROD - partition, and also analysis
    * Pad trigger logic
    * LTP/ATLI
    * Connection to external triggers
    * Configuration of LTP/Alti
* **Some preliminary studies:**
    * Also do test-pulse with fixed number of pulses
    * Studies with scope (as Xiong did)
    * Define incident angles
    * Peaking time can be sensitive to different noise frequencies.
    * Studies with scintillator
    * Play with the channel tragging and L0 offsets and find the correct combinations so that the rel-bcid is 3.
* **Tools needed to be developed:**
    * Netio decoding - mostly will use Rongkun's package. But have to do event building as Rongkun's script only writes packets according to the elinks and not L1 ids.
    * Write in NTuples
    * Tools on online monitoring of the data
    * Dealing with crashes and syncronization issues
* **Final topics:**
    * Rel BCID distributions
    * Yield versus amplitude and Yield versus tdo\
    (References for the above two: https://indico.cern.ch/event/734153/contributions/3031148/attachments/1663872/2666658/stgc_elx_meeting.pdf
    
    https://indico.cern.ch/event/757069/contributions/3219747/attachments/1764700/2865483/2018.12.04_sTGC_trigger_chain_LiangGuan.pdf)
    * Noise - Checking accidental hits (SCurve in readout)
    * S/N ratio and threshold decision and calibration (thresholds could be different for different channels)
    * Cross-talk - by checking the efficiencies of different layers
    * Knowing which layer gives the information first for 3/4 coincidences
    * tdo-pdo calibration
    * Studies with srat bit
    * Studies with changing the tdo charging slope
    
## Rongkun - b191 Setup

* **Why is Alti module preferred:**\
    Because it is just one module which generates TTC instead of having multiple modules. Length of the cables used for connections between different modules can affect the system performance and hence Alti is easier to handle.
* **About the partition**
    * OKS Database - .xml file for configuration of LTP/Alti.
    * A partition is created to control Alti, swROD and configuration of the front-ends and L1DDC.
* **Changes in NSWAlti.data.xml:**
    * input_l1a from PG to NIM_in <span style="color:orange">(not exactly sure)</span>.
    * slot has to be changed <span style="color:orange">(not exactly sure)</span>.
    * .dat file has to be changed <span style="color:orange">(not exactly sure)</span>.
* **Steps to take:**
    * Ask Xiong what is the output of the pad trigger logic <span style="color:green">(Already asked and it is a pulse+some other info. </span> <span style="color:orange">(Only pulse can be extracted from it by breaking the signals into different components (not sure which setup is used to do this)).</span>
    * Ask for another Alti (Will be the easiest option)
    * If not for LTP - Can ask Enrico's help
    * Add external L1A to the LTP/Alti. Everything else is mostly already connected.
    * Create partition: 
    (Reference: https://gitlab.cern.ch/atlas-muon-nsw-daq/nswpartitionmaker)
        * <ins>Set elinkdict (can be changed in ./python/ElinkDict.py)</ins>\
         Example dictionary entry: 
         " <span style="color:purple">9</span>: <span style="color:magenta">'NSW/L1A/sTGC/strip/SECTOR/L0/R12/G0'</span>". \
         Here:\
         **L0** - Layer number (starts from zero)\
         **R** - Radius which is basically quad number which also starts from 0. \
         **G** - group number ascociated with the R. Quad-1 has two groups and Quad-2 and QUad-3 have 1 each. And hence, \
         for GBTx1: \
             for Quad-1 - R0/G0, R0/G1\
             for Quad-2 and Quad-3 - R1/G0,R2/G0 \
         for GBTx2: \
             for Quad-1 - R0/G2, R0/G3\
             for Quad-2 and Quad-3 - R1/G1,R2/G1 \
         Here, from this dictionary, the string is then encoded to a 'detector id'. This detector id will be recorded everywhere (e.g. the NTuple.)
        * <ins>Settings in code: ./python/makeNSWPartition.py:</ins>\
          Do not add the dictionary for b180 to the code in function "prepareSite". Start from the b191 dictionary (in prepareSite) and then "replace" that one with the new one in "makeSegment". \
          We have to write dictionary separately for GBTx2 and then update the above dictionary in function "makeSegment".\
          In function "prepareSite", make appropriate changes to the server addresses.\
          In function "prepareSite", also add 'config["location"]' = 'b180'.
              * In config["location"], there is an option ```self.getConfig(self.config,"ttc.altiSlots", [4])```. In this '4' is the slot number of the VME crate which is used for Alti. e.g. for Alti in building 180 (felix3), the slot number is '10'.
              * 
        * <ins>Changes in file ./partitionConfigs/191A-Pulser.yml (create a separate file for b180 pulser test)</ins>\
          **readout:** and **config:** can be both used for LTP, however **ttc:** is specific to Alti. \
          **config:** Specify json file for configuration\
          **ttc:** if 'ttc.patternFile:' is not mentioned, then external trigger is used. 
              * About Pattern file: 
                the memory unit limit is 1mil. Each unit can save 2000 BC. so a 400 mil BC is 200k memory unit and you have 1000 repetition of things that takes 182 unit, which is 182k.
          **ttc.startPatternTransition: "PrepareForRun"** happens when you click "Start" in the gui.
          **elinks.sTGCOffset: 3**  - if this option is added while running felixcore, then it should be 3 for b180 as Alti is setup for felix3 for now. Check -h option while running felixcore.\
          **Storage of run data:**
          ```
          logroot: /data/stgcic/log_files/
          workingdirectory: /data/stgcic/work_dir/
          swrod.directorytowrite:  /data/stgcic/swrod/
          ```
          These directories should be created on Alti machine and swrod machine.\
          These directories should be owned by the user of the swrod machine (or the Alti machine). 
          ```chown stgcic <directory path>```
          ****
        * <ins>Set computer information in muons/hw/computers.data.xml.</ins> (Some details can be found here: https://unix.stackexchange.com/questions/218074/how-to-know-number-of-cores-of-a-system-in-linux, https://www.cyberciti.biz/faq/check-how-many-cpus-are-there-in-linux-system/
            * One way of getting this information is to use a script ```pm_seg_hlt.py```.
                * Go to the nswdaq account on the computer for which you need the information.
                * then run ```tdaq tdaq-09-02-01``` or the latest version of tdaq.
                * Then run ```pm_seg_hlt.py```. This creates a file HLT.data.xml. This file has the computer information. e.g.:
                  ```
                  <obj class="Computer" id="um-felix3.cern.ch">
                      <attr name="HW_Tag" type="enum" val="x86_64-centos7"/>
                      <attr name="Type" type="string" val="Intel(R) Xeon(R) CPU E5-1660 v4 @ 3.20GHz"/>
                      <attr name="State" type="bool" val="1"/>
                      <attr name="Memory" type="u32" val="24405"/>
                      <attr name="CPU" type="u16" val="1599"/>
                      <attr name="NumberOfCores" type="u16" val="16"/>
                      <attr name="RLogin" type="string" val="ssh"/>
                      <rel name="Interfaces">
                          <ref class="NetworkInterface" id="um-felix3.cern.ch-eno1"/>
                          <ref class="NetworkInterface" id="um-felix3.cern.ch-virbr0"/>
                      </rel>
                  </obj>
                  ```
                * Add two more lines to this after ```<attr name="Type" type="string" val="Intel(R) Xeon(R) CPU E5-1660 v4 @ 3.20GHz"/>``` For ```Location``` and ```Description```.
                   ```
                   <attr name="Location" type="string" val="180"/>
                   <attr name="Description" type="string" val="UM Felix 3"/>
                   ```
                * This method seems to be giving some different information compared to what is there already in the git repo. Not sure which one is correct. 
            * Information of the following machines being used is supposed to be added to the above file:
                * SBC machine
                * Felix machine
                * swrod machine
                
* **Some links:**
    * Old LTP setup: https://twiki.cern.ch/twiki/bin/view/Sandbox/Rongkun_swROD
    * Old LTP setup: cd /afs/cern.ch/user/n/nswdaq/public/db/sTGC_NSW_OKS_Test_DB
    * alti control in tdaq, using PartitionMaker https://gitlab.cern.ch/atlas-muon-nsw-daq/nswpartitionmaker/-/wikis/191-MM-Pulser-Instructions
    * tdaq tutorial https://indico.cern.ch/event/799489/
    * atlas-muon-nsw-daq/nswpartitionmaker#66
    * Basic TDAQ code: https://gitlab.cern.ch/atlas-tdaq-software/tdaq-cmake
    * How to install swROD (and other TDAQ related instructions): https://espace.cern.ch/NSWCommissioning/_layouts/15/start.aspx#/default.aspx?RootFolder=%2FNSWCommissioning%2FShared%20Documents%2FDAQ&FolderCTID=0x012000B7B070FB7D360746AF1EFA19BC5E2938&View=%7B0FF4EB01%2D4841%2D4873%2DB480%2D19E543BF74FF%7D

## Installation of LINUX CERN CENTOS7

* swrod machine used at this point (04/05/2021) atlas-nsw-stgc-daq1.

    There was a problem with mounting afs on the local swrod (like) machine. Hence, the hard-disk was changed and a new hard-disk was put for reinstalling CERN CENTOS7. The previous hard-disk is kept in the slots available near the felix machine at slot number 5. 

* (CERN IT department help was taken and the ticket is here: https://cern.service-now.com/service-portal?id=ticket&table=u_request_fulfillment&n=RQF1791352)

    On the new hard-disk, the following procedure was followed to install the system.
    * https://linux.web.cern.ch/installation/pxeboot/ (In step two also disable Secure Boot, if Secure Boot gives some error like "Operating system loader has no signature. Incompatible with secure boot"). https://www.intel.com/content/www/us/en/support/articles/000006678/boards-and-kits/desktop-boards.html - how to go to BIOS setup.
    * After steps 1,2,3, go to step 5. (https://linux.web.cern.ch/centos7/docs/install/). The instructions on how to mount afs and eos is here.
    * Adding new cern user: ```/usr/sbin/addusercern <cern-username>```
    * Logging in to that user account: ```su -l <cern-username>``` (https://phoenixnap.com/kb/su-command-linux-examples)

## Requesting a fixed IP address

The swrod machine should have a fixed IP registered. The partition created should be able to use Kerberos token to control. However, with dynamic ip (dyndns.cern.ch), it cannot be done. 

To do this, 
* Go to:
  https://network.cern.ch/sc/fcgi/sc.fcgi?Action=SelectForNewConnection
  
  Here you can select the machine you want to add fixed IP to.
  <img src='./images/swROD/adding_fixed_ip.png' width="" height="250"> 
* Then add the outlet number (marked as red underline). This number is a number corresponding to the ethernet source (See 'Network Service HELP' for more info. Marked as green circle.)
  <img src='./images/swROD/Add_outlet.png' width="" height="250"> 
* In some time the request will be approved.  

## Access using Kerberos and ssh

Access using kerberos is important for setting a secure connection between host anda client machine. This also allows the client to access the host without password while ssh ing.

About Kerberos access, read the following link:
https://linux.web.cern.ch/docs/kerberos-access/

If the file /etc/krb5.keytab does not exist (can check using ls -l), then follow this:
https://linux.web.cern.ch/docs/kerberos-access/#server-side-configuration-and-troubleshooting

About ssh:
Add the following lines to ~/.ssh/config or to /etc/ssh/ssh_config.
```
HOST *
     GSSAPITrustDns yes
     GSSAPIAuthentication yes
     GSSAPIDelegateCredentials yes
     PubkeyAuthentication yes
```

Reference:


## Jared Sturdy - About TDAQ/NSWDAQ releases

Detailed discussion here: https://gitlab.cern.ch/atlas-muon-nsw-daq/nswdaq/-/issues/50

* Always should try to use already built releases of ```nswdaq``` installed here: 
```/afs/cern.ch/user/n/nswdaq/workspace/public/nswdaq/current/nswdaq```
    * If you just want a "stable" release, you will choose ```installed``` (this is probably what you want).
    * ```dev-installed``` should be stable as well, but it is released without widespread full-scale testing, so it is released to ```dev-installed``` for about a week after the release is closed.
    * ```unstable-installed``` includes the packages that will become the next release, before they have been finalized.
    * (Probably) ```installed``` is then moved to ```old_installed``` after another stable release replaces ```installed```.

Another merge request has been created to merge the b180 changes to the master branch of NSWPartitionMaker: https://gitlab.cern.ch/atlas-muon-nsw-daq/nswpartitionmaker/-/merge_requests/121

## About ALTI

* Preliminary Design Review: https://indico.cern.ch/event/735376/
* PRR - https://indico.cern.ch/event/862997/
* ALTI Documentation: https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LevelOneCentralTriggerALTI
* Gitlab ALTI: https://gitlab.cern.ch/atlas-tdaq-software/ALTI
* Gitlab for AltiController: https://gitlab.cern.ch/atlas-tdaq-software/AltiController
* CERN Thesis on ALTI: https://cds.cern.ch/record/2655027/files/CERN-THESIS-2018-335.pdf

## About Information Service

Some links:
* https://atlas-tdaq-monitoring.web.cern.ch/IS/Welcome.htm
* User Guide: https://atlas-tdaq-monitoring.web.cern.ch/IS/doc/userguide/html/is-usersguide.html
* ```is_monitor``` infor: https://atlas-tdaq-monitoring.web.cern.ch/IS/doc/userguide/html/is-usersguide-45.html

## Starting and stopping a partition

* Opening ```IGUI``` and starting the partition:
    ```
    ssh -Y stgcic@atlas-nsw-stgc-daq1 
    source /afs/cern.ch/work/s/stgcic/public/swROD/nswpartitionmaker_work/partitionConfigs/180-sTGC_Pulser/setup_part.sh
    run
    ```
* Closing the IGUI and shutting down the partition:
    Go to the ```File``` menu in the ```IGUI``` and then select ```Close IGUI & exit partition```. 
    Never select the ```x``` in the top right corner. However if you do that, then run ```run``` again and gain control of the system by going to ```Access Control --> Control (instead of Display)``` 
* Killing the partition (abnormal ways to shut down the partition. Try not to do it.) \
    ```pmg_kill_partition -p part-180-sTGC_Pulser; pkill -ec -f part-180-sTGC_Pulser; rmgr_free_partition_resources -p part-180-sTGC_Pulser```

* Instructions for changing some parameters by not closing the IGUI or the partition:
    * https://stgc-comm-docs.web.cern.ch/elx/noise_test/

## Debugging 

* ```sudo tail -f /var/log/secure```
    This is to check if the connection is being established with the machine. Run it on the machine that is giving you issues.

## Enrico - Debugging the VME crate and ALTI setup on SBC machine

* Problems:
    * Some problem with VME errors because the transmitters are switched ON during the ```/home/alti/ALTI_power_up.expect``` setup but the alti is not initialized.
    * SELinux and firewall were enabled on sbc machine.
    * The drivers were installed with tdaq release of 09-01-00. Current release at this time (2021-06-03) is tdaq-09-02-01.

* Solutions:
    * In file ```/etc/init.d/drivers_tdaq```: The driver path is set to ```/ATLAS/current``` so the driver versions will be updated evertime the system is booted. Message from Enrico: "It was already configured to start at boot, so I just updated it to get the files (configuration, setup, direvers, executables) from ```/ATLAS/current```. ```current``` is now a link to the directory corresponding to the correct tdaq version under ```/ATLAS```".
    * ALTI initialisation and transmitter configuration are started via systemctl (/etc/systemd/systemctl/tdaq_l1....). 
    * Enrico's message: Also, the tdaq installation is on cvmfs, which is in automount, so, tdaq setup was not found by the latter service. I modified the script that calls it under /ATLAS/current to test the director until it is available, and now everything works at boot.So, now you do not need to init alti or start transmitters manually. Just call the partition setup script and run. There another small problem with OKS configuration. That may involve some partition maker minor modification. Let me make some more test with that.
    * Message from Enrico: The procedure in the wiki work for the file system and services as they are at P1. The problem we had with ours is different: it did not work properly for a couple of reasons:
        * 1) dependencies did not work because ```[unit]``` was specified instead of ```[Unit]``` 
        * 2) cvmfs is auto mounted. So the first time we try to use a directory it is not found. 
      At least, /cvmfs is not mounted properly when the service starts. In order to fix that I slightly modified the tdaq setup under ```/ATLAS/current``` to test the directory where the tdaq setup is until it appears. I also did something else. For me it is always painful to dig for files around where they are sparse more or less random. It was the case for the expect script turning on the ALTI transmitters. I just moved it to ```/ATLAS/current/bin```, so can transfer all the initial setup of the machine (tdaq drivers and alti setup) to another SBC, provided we remember to check that the correct vmetab is available. So: nothing is now needed under /opt. No manual setup is needed before running, except the tdaq and partition in the setup of the partition itself.

## Problem in configuration of the boards through IGUI

The error is as follows:

<img src='./images/swROD/swrod_configuration_error.png' width="" height="250">

The error reads as :
```
The transition connect has not been properly completed.
Bad address '' for e-link 0x49 uuid = ''
```

This error was because of the wrong broadcast IP for um-felix3 and was solved by changing ```ipv4.method``` to ```auto``` from ```manual```. 

```
nmcli con show eno1

Output:
ipv4.method:                            manual
ipv4.dns:                               137.138.16.5,137.138.17.5
ipv4.dns-search:                        --
ipv4.dns-options:                       ""
ipv4.dns-priority:                      0
ipv4.addresses:                         128.141.179.243/32
ipv4.gateway:                           128.141.179.1
ipv4.routes:                            --
ipv4.route-metric:                      -1
ipv4.route-table:                       0 (unspec)
ipv4.routing-rules:                     --
ipv4.ignore-auto-routes:                no
ipv4.ignore-auto-dns:                   no
ipv4.dhcp-client-id:                    --
ipv4.dhcp-timeout:                      0 (default)
ipv4.dhcp-send-hostname:                yes
ipv4.dhcp-hostname:                     --
ipv4.dhcp-fqdn:                         --
ipv4.never-default:                     no
ipv4.may-fail:                          yes
ipv4.dad-timeout:                       -1 (default)

```

```
nmcli con edit 802-3-ethernet
nmcli> set ipv4.method auto
nmcli> Do you also want to clear 'ipv4.addresses'? [yes]: yes
nmcli> remove ipv4.gateway
nmcli> save
Connection 'eno1' (911e32fd-4be5-42a1-9766-ff9017bff9a7) successfully updated.
nmcli> q

sudo systemctl restart network
```

After this change after hitting ```CONFIG``` in IGUI, we see felixcore subscriptions to the data elinks.

## Analysing the data from the swrod run

The options for analysing the swrod raw output:

* NSWRead: 
    * https://gitlab.cern.ch/atlas-muon-nsw-daq/NSWRead
    * https://twiki.cern.ch/twiki/bin/viewauth/Atlas/NSWRead
* Some swrod data format excel sheet: https://espace.cern.ch/ATLAS-NSW-ELX/_layouts/15/WopiFrame2.aspx?sourcedoc=/ATLAS-NSW-ELX/Shared%20Documents/DAQ/NSW_DataHandlerOutputFormat.xlsx&action=default
* By using eformat libraries: https://gitlab.cern.ch/atlas-tdaq-software/eformat
* Documentation for the raw data format: https://edms.cern.ch/ui/#!master/navigator/document?D:100616566:100616566:subDocs

## Setting up CAEN Power supply modules

* Manual for the module:
    * N1470: https://aisdb.cern.ch/pls/htmldb_aisdb_prod/f?p=189:7:5874785915022::NO:RP:P7_ITEM:0422
    * 
* Manual for the remote control gui:
    * https://www.caen.it/download/?filter=GECO2020
* For starting the remote control gui:
    * From the power supply set the ```Control``` to ```Remote```.
    * The connected channels on the module to ```ON``` state physically (The status you will see on the power supply screen will say ```OFF```).
    * Connect the power supply to the computer with USB connection. 
    * Check the port number using the following command on the terminal: ```dmesg | grep tty```. The port number should look like ttyUSBX (X is a number).
    * Install the CAENGECO2020 package for the gui. 
    * Run ```CAENGECO2020```mostly in ```su```.  
    * Then fill in all the information needed:
        <img src='./images/CAENGui_parameters.png' width="" height="250"> 
        
        * You can check the other parameters on the power supply by going to each parameter one by one. (see the power supply manual).
    * Follow the manual for further info on how to control the high voltage.
    
## Setting offsets for L0 and L1 matching:
* Refer to the presentation here: https://docs.google.com/a/umich.edu/viewer?a=v&pid=sites&srcid=dW1pY2guZWR1fG5zdy1zdGdjLWludGVncmF0aW9uLWFuZC1jb21taXNzaW9uaW5nfGd4OjFmNTExNGRjZjUwZjVlODE
    * Another suggestion apart from the ones given in the presentation above: 
        * If you want to change the difference between ch tagging offset and L0 offset for specific boards, do not change the L0 offset, change the channel tagging offset. If swrod receives data with different l1/l0 ids, then it might lose some events.
* How to set BCID offset of felix:
  * ttc elink of felix: hex(827) '0x33b'\
  * setting the bcid offset of felix: flx-config -d 0 set TTC_DEC_CTRL_BCID_ONBCR=0x383\
  * 383 is the value, you can tune it \
  * ttc data from felix looks like : https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-user-manual/versions/4.0.6/C_datastructures.html#_11__guide_to_felix_data_structures \
  * change TTC_DEC_CTRL_BCID_ONBCR accordingly until the bcid between 827 and FE matches
  * also change swrod.ttcelink: 0x6033B in yml according to the actual ttc elink\
  * Link that might be useful for this: https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-user-manual/versions/4.0.6/C_datastructures.html