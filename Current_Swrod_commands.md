### How to generate a partition

```
cd /afs/cern.ch/user/n/nswdaq/workspace/public/stgc_b180/nswpartitionmaker/partitionConfigs/
source /afs/cern.ch/user/n/nswdaq/workspace/public/nswdaq/current/nswdaq/dev-installed/setup_pm.sh
makeNSWPartition.py 180-sTGC_Pulser_ExtL1_swrod.yml (or any yml file you want)
```


### How to start IGUI

```
ssh -Y nswdaq@um-felix3-swrod
cd /afs/cern.ch/user/n/nswdaq/workspace/public/stgc_b180/nswpartitionmaker/partitionConfigs/180-sTGC_Pulser_ExtL1_swrod
source setup_part.sh
run
```

Closing the IGUI and shutting down the partition:
    Go to the ```File``` menu in the ```IGUI``` and then select ```Close IGUI & exit partition```. 
    Never select the ```x``` in the top right corner. However if you do that, then run ```run``` again and gain control of the system by going to ```Access Control --> Control (instead of Display)``` 
* Killing the partition (abnormal ways to shut down the partition. Try not to do it.) \
    ```pmg_kill_partition -p part-180-sTGC_Pulser; pkill -ec -f part-180-sTGC_Pulser; rmgr_free_partition_resources -p part-180-sTGC_Pulser```

* Instructions for changing some parameters by not closing the IGUI or the partition:
    * https://stgc-comm-docs.web.cern.ch/elx/noise_test/
    
    
### How to analyze the cosmic run:
    
### How to take noise scan:

### How to take pdo-tdo calibration: